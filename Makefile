NAME		= pipex
SRCS		= srcs/main.c srcs/utils.c srcs/pipe.c srcs/utils_bis.c srcs/file.c srcs/error.c
SRCB		= srcs/main_bonus.c srcs/here_doc_bonus.c
OBJS		= $(SRCS:.c=.o)
OBJB		= $(SRCB:.c=.o)
LIBFT		= srcs/libft/libft.a
CC			= clang
CFLAGS		=  -fsanitize=address -g -Wall -Wextra -Werror
INCLUDE		= -I srcs/data.h -I srcs/utils.h -I srcs/pipe.h -I srcs/file.h -I srcs/error.h -I srcs/here_doc_bonus.c


all :		${NAME}
.PHONY :	all

%.o :		%.c
			${CC} ${CFLAGS} -c $< -o ${<:.c=.o} ${INCLUDE}

${NAME} :	${OBJS}
			make -C srcs/libft -f Makefile
			${CC} ${CFLAGS} ${OBJS} ${LIBFT} ${INCLUDE} -o ${NAME}

clean :
			rm -f ${OBJS} ${OBJB}
			make fclean -C srcs/libft -f Makefile
.PHONY:		clean

fclean :	clean
			rm -f ${NAME}
.PHONY :	fclean

re :		fclean all
.PHONY :	re

bonus :		CFLAGS += -D BONUS
bonus :		${OBJS} ${OBJB}
			make -C srcs/libft -f Makefile
			${CC} ${CFLAGS} ${OBJS} ${OBJB} ${LIBFT} ${INCLUDE} -o ${NAME}
.PHONY:		bonus
