/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipe.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/29 17:21:18 by gcolomer          #+#    #+#             */
/*   Updated: 2021/07/29 17:21:18 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIPE_H
# define PIPE_H

# include "data.h"
# include <stdlib.h>

# define RDWR_USR 0644

int		execution(t_data *data, char *env[], int nb_proc);
int		load_cmd(t_data *data, char *env[], char *one_cmd);
int		all_pipe(int x_pipe, int **fd, t_data *data);
int		all_waitpid(int x_waitpid, int *pid);

#endif
