/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipe.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/29 17:11:02 by gcolomer          #+#    #+#             */
/*   Updated: 2021/07/29 17:11:02 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipe.h"
#include "utils.h"
#include "file.h"
#include "libft/libft.h"
#include "error.h"

#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>

/* Execute all pipe					*
 * Open infile and outfile			*
 * x_pipe = number of processus - 1 */
int	all_pipe(int x_pipe, int **pipefd, t_data *data)
{
	int	i;

	i = 0;
	if (data->heredoc.v == NO)
		data->file[0] = open(data->name_infile, O_RDONLY);
	else
		data->file[0] = open("tmp", O_RDONLY);
	data->file[1] = open(data->name_outfile, O_RDWR | O_CREAT
			| O_TRUNC, RDWR_USR);
	if (data->file[0] == ERROR || data->file[1] == ERROR)
		return (error("pipe.c [39]"));
	while (i < x_pipe)
	{
		if (pipe(pipefd[i]) == ERROR)
			return (error("pipe.c [43]"));
		i++;
	}
	return (SUCCESS);
}

/* x_waitpid = number of processus - 1 */
int	all_waitpid(int x_waitpid, int *pid)
{
	int	i;

	i = 0;
	while (i < x_waitpid)
	{
		if (pid[i] > 0)
		{
			if (waitpid(pid[i], 0, 0) < 0)
				return (error("pipe.c [61]"));
		}
		i++;
	}
	return (SUCCESS);
}

/* Set variable for execve and execute cmd */
int	load_cmd(t_data *data, char *env[], char *cmd)
{
	char	**cmd_arg;
	int		i;

	i = 0;
	data->cmd_path = get_input_cmd(cmd, env, data);
	cmd_arg = ft_split(data->cmd_opt, ' ');
	if (cmd_arg == NULL || data->cmd_path == NULL)
		exit (error("pipe.c [71]"));
	if (execve(data->cmd_path, cmd_arg, env) == ERROR)
	{
		while (cmd_arg[i])
		{
			free(cmd_arg[i]);
			i++;
		}
		return (error("pipe.c [86]"));
	}
	return (SUCCESS);
}

/* when i use <nb_proc - 1>, its for array indice */
int	execution(t_data *data, char *env[], int nb_proc)
{
	int	i;

	i = 0;
	set_variable(&data->pipefd, &data->pid, nb_proc);
	if (all_pipe(nb_proc - 1, data->pipefd, data) == ERROR)
		return (ERROR);
	while (i < nb_proc)
	{
		data->pid[i] = fork();
		if (data->pid[i] == ERROR)
			return (error("pipe.c [103]"));
		if (data->pid[i] == 0)
			manage_processus(data, i, env);
		else if (i < nb_proc - 1)
			close(data->pipefd[i][STDOUT]);
		i++;
	}
	if (all_waitpid(nb_proc - 1, data->pid) == ERROR)
		return (ERROR);
	free(data->pid);
	free_matrice(data->pipefd, nb_proc - 1);
	if (data->heredoc.v == YES)
		unlink("tmp");
	return (SUCCESS);
}
