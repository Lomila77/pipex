/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/15 16:17:49 by gcolomer          #+#    #+#             */
/*   Updated: 2021/08/15 16:17:49 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "file.h"
#include "utils.h"
#include "pipe.h"
#include "error.h"

#include <fcntl.h>
#include <unistd.h>

/* For the first cmd in argv */
int	first_processus(t_data *data, char *env[])
{
	if (dup2(data->file[STDIN], STDIN) == ERROR)
		return (error("file.c [25]"));
	if (dup2(data->pipefd[0][STDOUT], STDOUT) == ERROR)
		return (error("file.c [27]"));
	close(data->pipefd[0][STDIN]);
	if (load_cmd(data, env, data->all_cmd[0]) == ERROR)
		return (ERROR);
	return (SUCCESS);
}

/* For the last cmd in argv */
int	last_processus(t_data *data, int i, char *env[])
{
	if (dup2(data->pipefd[i - 1][STDIN], STDIN) == ERROR)
		return (error("file.c [38]"));
	if (dup2(data->file[STDOUT], STDOUT) == ERROR)
		return (error("file.c [40]"));
	if (load_cmd(data, env, data->all_cmd[i]) == ERROR)
		return (ERROR);
	return (SUCCESS);
}

/* For others cmd in argv */
int	std_processus(t_data *data, int i, char *env[])
{
	if (dup2(data->pipefd[i - 1][STDIN], STDIN) == ERROR)
		return (error("file.c [50]"));
	if (dup2(data->pipefd[i][STDOUT], STDOUT) == ERROR)
		return (error("file.c [52]"));
	close(data->pipefd[i - 1][STDIN]);
	if (load_cmd(data, env, data->all_cmd[i]) == ERROR)
		return (ERROR);
	return (SUCCESS);
}

int	manage_processus(t_data *data, int i, char *env[])
{
	if (i == 0)
	{
		if (first_processus(data, env) == ERROR)
			return (ERROR);
	}
	else if (i == data->nb_proc - 1)
	{
		if (last_processus(data, i, env) == ERROR)
			return (ERROR);
	}
	else
	{
		if (std_processus(data, i, env) == ERROR)
			return (ERROR);
	}
	return (SUCCESS);
}
