/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   here_doc_bonus.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/29 10:59:35 by gcolomer          #+#    #+#             */
/*   Updated: 2021/08/29 10:59:35 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "here_doc_bonus.h"
#include "libft/libft.h"
#include "data.h"
#include "pipe.h"
#include "error.h"

#include <unistd.h>
#include <fcntl.h>

/* write in tmp all line readed and close fd for reset the cursor */
int	set_input(char *line, t_data *data, char *limiter_find)
{
	int	nb_oct;

	nb_oct = limiter_find - line;
	if (write(data->heredoc.fd, line, nb_oct) == ERROR)
		return (error("here_doc_bonus.c [29]"));
	close(data->heredoc.fd);
	ft_free(&line);
	return (SUCCESS);
}

/* read stdin and return all line readed */
char	*get_all_line(char *txt[3], t_data *data, int *read_return)
{
	*read_return = read(0, txt[1], 8);
	if (*read_return == ERROR)
		return (ERROR_N);
	txt[2] = txt[0];
	txt[0] = ft_strjoin(txt[2], txt[1]);
	ft_free(&txt[2]);
	ft_bzero(txt[1], 8 + 1);
	return (ft_strnstr(txt[0], data->heredoc.limiter,
			ft_strlen(txt[0])));
}

/* Check if the limiter has finded and call respective function */
int	here_doc(t_data *data)
{
	char	*limiter_find;
	int		read_return;
	char	*txt[3];

	limiter_find = NULL;
	txt[0] = NULL;
	txt[2] = NULL;
	read_return = 1;
	txt[1] = ft_calloc(8 + 1, sizeof(char));
	data->heredoc.fd = open("tmp", O_RDWR | O_CREAT | O_TRUNC, RDWR_USR);
	if (data->heredoc.fd == ERROR)
		return (error("here_doc_bonus.c [60]"));
	while (read_return > 0)
	{
		limiter_find = get_all_line(txt, data, &read_return);
		if (txt[0] == ERROR_N)
			return (error("here_doc_bonus.c [65]"));
		if (limiter_find != ERROR_N)
			return (set_input(txt[0], data, limiter_find));
	}
	return (error("here_doc_bonus.c [69]"));
}
