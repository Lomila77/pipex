/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/27 16:27:29 by gcolomer          #+#    #+#             */
/*   Updated: 2021/07/27 16:27:29 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipe.h"
#include "data.h"
#include "libft/libft.h"
#include "error.h"

#include <stdio.h>
#include <string.h>

#ifndef BONUS

int	main(int argc, char **argv, char *env[])
{
	t_data	data;

	if (argc != 5)
		return (error("main.c [25]"));
	data.heredoc.v = NO;
	data.cmd_path = NULL;
	data.name_infile = argv[1];
	data.name_outfile = argv[argc - 1];
	data.all_cmd = &argv[2];
	data.nb_proc = argc - 3;
	if (execution(&data, env, data.nb_proc) == ERROR)
		return (ERROR);
	return (0);
}
#endif
