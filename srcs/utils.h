/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/29 15:22:29 by gcolomer          #+#    #+#             */
/*   Updated: 2021/07/29 15:22:29 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTILS_H
# define UTILS_H

# include "data.h"
# include "libft/libft.h"

# include <sys/types.h>
# include <sys/wait.h>

# define FIRST		0
# define LAST		1
# define NO_FLAGS	2
# define PATH		0
# define NAME		1

char	*get_path(char *env[]);
char	**get_allpath(char *env[]);
char	*cut_cmd(char *cmd_name);
int		free_matrice(int **matrice, int len);
int		set_variable(int ***fd, pid_t **pid, int x);
char	*get_input_cmd(char *cmd_opt, char *env[], t_data *data);

#endif
