/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_bis.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/14 15:57:17 by gcolomer          #+#    #+#             */
/*   Updated: 2021/08/14 15:57:17 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"
#include "error.h"

#include <fcntl.h>
#include <unistd.h>

/* Malloc variable */
int	set_variable(int ***pipefd, pid_t **pid, int nb_proc)
{
	int	i;

	i = 0;
	(*pipefd) = malloc(sizeof(int *) * (nb_proc - 1));
	if ((*pipefd) == NULL)
		return (error("utils_bis.c [26]"));
	while (i < nb_proc - 1)
	{
		(*pipefd)[i] = ft_calloc(2, sizeof(int));
		if ((*pipefd)[i] == NULL)
			return (error("utils_bis.c [32]"));
		i++;
	}
	*pid = malloc(sizeof(pid_t) * nb_proc);
	if (*pid == NULL)
		return (error("utils_bis.c [36]"));
	return (SUCCESS);
}

int	need_path(char ***path, char *env[], int *path_ok)
{
	*path = get_allpath(env);
	if (*path == NULL)
		return (ERROR);
	*path_ok = YES;
	return (SUCCESS);
}

/* get the absolute path of cmd with and without options */
char	*get_input_cmd(char *cmd_opt, char *env[], t_data *data)
{
	static int	path_ok = NO;
	static char	**path;
	char		*cmd[2];
	int			access_return;
	int			i;

	i = 0;
	access_return = -1;
	cmd[PATH] = NULL;
	cmd[NAME] = cut_cmd(cmd_opt);
	ft_free(&data->cmd_path);
	if (path_ok == NO)
		if (need_path(&path, env, &path_ok) == ERROR)
			return (ERROR_N);
	while (access_return < 0 && path[i])
	{
		cmd[PATH] = ft_strjoin_sep(path[i], (char *)cmd[NAME], '/');
		data->cmd_opt = ft_strjoin_sep(path[i], (char *)cmd_opt, '/');
		access_return = access(cmd[PATH], X_OK);
		i++;
	}
	if (access_return == ERROR || cmd_opt[0] == '\0')
		return (ERROR_N);
	free(cmd[NAME]);
	return (cmd[PATH]);
}
