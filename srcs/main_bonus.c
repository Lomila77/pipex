/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_bonus.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/19 15:35:46 by gcolomer          #+#    #+#             */
/*   Updated: 2021/08/19 15:35:46 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipe.h"
#include "data.h"
#include "libft/libft.h"
#include "here_doc_bonus.h"
#include "error.h"

#ifdef BONUS

/* set variable for bonus "multipipe" with "here_doc" */
int	set_here_doc(char **argv, t_data *data, int argc)
{
	if (argc < 6)
		return (error("main_bonus.c [33]"));
	data->heredoc.v = YES;
	data->nb_proc = argc - 4;
	data->heredoc.limiter = argv[2];
	data->all_cmd = &argv[3];
	return (SUCCESS);
}

/* set variable for bonus "multipipe" without "here_doc" */
void	set_no_here_doc(char **argv, t_data *data, int argc)
{
	data->heredoc.v = NO;
	data->name_infile = argv[1];
	data->all_cmd = &argv[2];
	data->nb_proc = argc - 3;
}

/* If bonus rules is define, execute this code : */
int	main(int argc, char **argv, char *env[])
{
	t_data	data;

	if (argc < 5)
		return (error("main_bonus.c [28]"));
	if (ft_strncmp(argv[1], "here_doc", 8) == 0)
	{
		if (set_here_doc(argv, &data, argc) == ERROR)
			return (ERROR);
		here_doc(&data);
	}
	else
		set_no_here_doc(argv, &data, argc);
	data.name_outfile = argv[argc - 1];
	data.cmd_path = NULL;
	if (execution(&data, env, data.nb_proc) == ERROR)
		return (ERROR);
	return (0);
}
#endif
