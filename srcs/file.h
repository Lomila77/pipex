/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/15 16:20:16 by gcolomer          #+#    #+#             */
/*   Updated: 2021/08/15 16:20:16 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILE_H
# define FILE_H

# include "data.h"

int	manage_processus(t_data *data, int i, char *env[]);
int	std_processus(t_data *data, int i, char *env[]);
int	first_processus(t_data *data, char *env[]);
int	last_processus(t_data *data, int i, char *env[]);

#endif
