/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/27 16:38:45 by gcolomer          #+#    #+#             */
/*   Updated: 2021/07/27 16:38:45 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DATA_H
# define DATA_H

# include <sys/types.h>
# include <sys/wait.h>

typedef struct s_heredoc
{
	int		v;
	int		fd;
	char	*limiter;
}	t_heredoc;

typedef struct s_data
{
	char		*cmd_path;
	char		*cmd_opt;
	char		*name_infile;
	char		*name_outfile;
	char		**all_cmd;
	int			**pipefd;
	int			file[2];
	int			nb_proc;
	pid_t		*pid;
	t_heredoc	heredoc;
}			t_data;

#endif
