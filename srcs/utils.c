/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/29 15:21:48 by gcolomer          #+#    #+#             */
/*   Updated: 2021/07/29 15:21:48 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"
#include "libft/libft.h"
#include "error.h"

#include <stdlib.h>
#include <unistd.h>

/* Get the path in the environnement give bye the main	*
 * And return all path without "PATH="					*/
char	*get_path(char *env[])
{
	int	i;

	i = 0;
	if (env[i] == NULL)
		return (NULL);
	while (ft_strncmp(env[i], "PATH=", 5) != 0)
	{
		i++;
		if (env[i] == NULL)
			return (NULL);
	}
	env[i] += ft_strlen("PATH=");
	return (env[i]);
}

/* Separe all path in multiple array */
char	**get_allpath(char *env[])
{
	char	*path;
	char	**all_path;
	int		sep;
	int		i;

	path = get_path(env);
	sep = count_char(path, ':') + 1;
	all_path = ft_calloc(sep + 1, sizeof(char *));
	i = 0;
	while (count_char(path, ':') != 0)
	{
		sep = ft_count(':', path);
		all_path[i] = ft_strccpy(path, ':');
		path += sep + 1;
		i++;
	}
	all_path[i] = ft_strccpy(path, '\0');
	return (all_path);
}

/* return the command without his option */
char	*cut_cmd(char *cmd_arg)
{
	if (str_have_space(cmd_arg))
	{
		return (ft_substr(cmd_arg, 0, ft_count(' ', cmd_arg)));
	}
	return (cmd_arg);
}

/* free a multiple array */
int	free_matrice(int **matrice, int len)
{
	int	i;

	i = 0;
	if (matrice[i] == NULL)
		return (error("utils.c [79]"));
	while (i < len)
	{
		free(matrice[i]);
		i++;
	}
	free(matrice);
	return (SUCCESS);
}
